# C# StyleCopによるコーディング規約詳細説明

{{toc}}

## 本資料の内容

ここでは、Visual Studioの拡張ツール「StyleCop（スタイルコップ）」の利用方法と設定内容について説明する。記載内容は一般的な日本語環境での開発を想定した項目に絞られている。StyleCopの全設定項目についての説明ではないことに注意すること。
 
---

## StyleCopのインストール

Nugetパッケージマネージャーコンソールから下記のコマンドを実行する。

```
Install-Package StyleCop.MSBuild -Version 5.0.0
```

また、単独のインストーラーが入手できている場合は、Visual Studio 2017が閉じている状態で.vsixファイルをダブルクリックで実行することでも導入できる。

---

## 利用方法

### 設定

- StyleCopの設定は、ソリューションエクスプローラーのプロジェクト部分を右クリックし、「StyleCop Settings」を選択して呼び出す。<br/><br/>

![StyleCop Settings](image_StyleCopSettings.png)

- 設定はプロジェクト単位が原則だが、プロジェクトの親フォルダであるソリューションファイルのあるフォルダ（*.slnがあるフォルダ）に設定情報を配置しておけば、全プロジェクトにその設定を反映させられる。この機能をONにするには、StyleCop Settingsから[Settings Files]-[Merge with settings file found in parent folders]をONにする（初期設定でON）。また、この場合は各プロジェクトの設定ファイル（Settings.StyleCop）はいったん削除しておいた方がよい。<br/><br/>

![StyleCop Settings Merge Parent Folder Settings Option](image_StyleCopSettings_SettingsFilesOption.png)

- 開発プロジェクトで利用する設定は別途提供する「Settings.StyleCop」ファイルに記載してあるので、このファイルをチェック対象ソースのソリューションファイルと同じフォルダに配置すること。

### 実行

- StyleCopは、ソリューションエクスプローラー、またはソースエディター上でマウス右クリックから「Run StyleCop」を選択して実行する。<br/><br/>

![Run StyleCop](image_RunStyleCop.png)

- 実行結果は専用の表示部分に列挙されるので、それぞれのメッセージをクリックし、該当部分のソースを見比べながらチェック結果がゼロになるまで実行と修正を繰り返す。
- なお、エラーが1,000件を超えるとそこでチェックが停止するので、適宜エラーを減らしてから再度実行すること。

---

# 開発プロジェクト用設定

以下では、開発プロジェクトで採用するStyleCop設定を列挙する。

## Documentation Rules（コメント等の記述関連ルール）

### Element Documentation（コメント要素の記述）

- SA1600: Elements must be documented.  
  ソースコード上の各要素にはXMLコメントを記述しないといけない。ただし、プライベート宣言されたフィールド（クラス内で利用可能な変数）については対象外とする。

- SA1601: (利用しない)

- SA1602: Enumeration items must be documented.  
  列挙型に関するXMLコメントを記述しなければならない。下記のように、型の宣言だけではなく各要素についても選択肢の説明を記載すること。  

  ```cs
  /// <summary>
  /// 並べ替え方向に関する列挙型。
  /// </summary>
  private enum SortDirection
  {
      /// <summary>
      /// 昇順。
      /// </summary>
      Ascending = 0,
  
      /// <summary>
      /// 降順。
      /// </summary>
      Descending = 1
  }
  ```

- SA1603: Documentation must contain valid xml.  
  XMLコメントのタグは定められた適切なものでないといけない。タグのスペルミスなどがある場合にこの指摘が行われる。

- SA1604: Element Documentation must have summary.  
  XMLコメントにはsummary（サマリー、要約）タグがないといけない。

- SA1605: Partial element documentation must have summary.  
  パーシャルクラス（partial class）を作成する場合、複数に分割されたそれぞれのクラスについてsummaryコメントを付記しないといけない。ただし、このルールはStyleCopの初期設定では *.designer.cs ファイルを対象から除外していることから、Formクラスに合わせて自動生成されるデザイナーファイルは対象外となる。

- SA1606: Element documentation must have summary text.  
  XMLコメントのsummaryタグの内容が空であってはいけない。コメントが記載されていない場合にこの規約に違反する。

- SA1607: Partial element documentation must not have default summary.  
  パーシャルクラスのXMLコメントのうち、summaryまたはcontentタグの内容が初期状態のままではいけない。この規約もSA1605同様に手作業でパーシャルクラスを作成した場合に適用される。なお、この規約ではsummaryタグまたはcontentタグに適切な文字列が記載されていれば問題ない。summaryタグはSandcastleのようなツールでドキュメントとして採用されるが、contentタグは採用されないことから、パーシャルの「副次的な方」はcontent、「主な処理の方」はsummaryでコメントを記載し、ドキュメント生成時のコメント衝突を回避するという手法も利用できるので検討すること（そうしないとパーシャルクラスの説明文がおかしなことになるケースがある）。

- SA1608: Element documentation must not have default summary.  
  XMLコメントの要素がVisual Studioが生成した初期状態のままになっていてはいけない。これはGhostDocなどで生成したテンプレートを利用していれば該当しない規約だが、既存コードなどでコメントが不十分な場合に発生する可能性があるため適宜訂正すること。

- SA1609: (利用しない)

- SA1610: (利用しない)

- SA1611: Element parameters must be documented.  
  引数に関するコメントが記載されていないといけない。メソッドなどのコメントでは引数に応じたコメントテンプレートが生成されるので、それぞれの引数について説明を記載すること。以下は引数や戻り値を含めたコメント例。

  ```cs
  /// <summary>
  /// ディレクトリを作成。
  /// </summary>
  /// <param name="path">作成するディレクトリパス文字列。</param>
  /// <returns>作成したディレクトリ情報。</returns>
  public DirectoryInfo SafeCreateDirectory(string path)
  {
      if (Directory.Exists(path))
      {
          return null;
      }
      return Directory.CreateDirectory(path);
  }
  ```

- SA1612: Element parameter documentation must match element parameters.  
  引数に関するコメント（paramタグ）の引数名は実際のメソッド宣言と一致していないといけない。

- SA1613: Element parameter documentation must declare parameter name.  
  引数に関するコメント（paramタグ）の引数名が空になっていてはいけない。

- SA1614: Element parameter documentation must have text.  
  引数に関するコメント（paramタグ）の説明文が空であってはいけない。

- SA1615: Element return value documentation must have text.  
  戻り値に関するコメント（returnsタグ）の説明文が空であってはいけない。

- SA1617: Void return value must not be documented.  
  戻り値がvoidの場合、戻り値に関するコメントを記載してはいけない。下記は戻り値がvoidの場合のコメント例。コメントにreturnsタグがない点に注目。

  ```cs
  /// <summary>
  /// ログ書き出し。
  /// </summary>
  /// <param name="result">ダイアログの選択結果。</param>
  private static void WriteLog(DialogResult result)
  {
      Log.WriteInformation("メッセージ戻り値：" + result.ToString());
  }
  ```

- SA1618: Generic type parameters must be documented.  
  ジェネリック型を扱う場合のコメントでは、ジェネリック型に関する説明を記載しないといけない。GhostDocを用いた場合、下記のような「typeparam」タグが自動生成されるので、型に関する説明書きを書き添えること。

  ```cs
  /// <summary>
  /// 指定したデータセットから値列に該当する情報をList型で取得する。
  /// </summary>
  /// <typeparam name="T">値列のデータ型。</typeparam>
  /// <param name="ds">データ取得対象データセット。</param>
  /// <returns>値列の情報を抜き出したListオブジェクト。</returns>
  private List<T> GetValues<T>(DataSet ds)
  {
      List<T> result = new List<T>();
  ```

- SA1619: Generic type parameter must be documented partial class.  
  ジェネリック型を引数に持つパーシャルクラス（partial class）の宣言において、XMLコメントのtypeparam文は両方のクラスに記述しないといけない。片方だけに記述する場合、他方のsummaryをcontentタグに変更すること。

- SA1620: Generic type paramter documentation must match type parameters.  
  ジェネリック型を引数に持つメソッドやクラスのXMLコメントは、必ず登場するジェネリック型すべてについてのtypeparamタグと説明文が記載されていないといけない。

- SA1621: Generic type parameter documentation must declare parameter name.
  ジェネリック型を引数に持つメソッドやクラスのXMLコメントにおいて、typeparamタグの「name」属性は空文字にしてはいけない。名前は必ず記載する必要がある。

- SA1622: Generic type parameter documentation must have text.  
  ジェネリック型を引数に持つメソッドやクラスのXMLコメントにおいて、typeparamタグの説明文は空欄にしてはいけない。

- SA1623: (利用しない)

- SA1624: (利用しない)

- SA1625: Element documentation must not be copied and pasted.  
  XMLコメントの文章をコピー＆ペーストで作成してはいけない。具体的には、1つのメソッドやクラスのコメントにおいて、複数の引数等のコメント文を全く同じしてはいけない。

  ```cs
  // 規約違反の例
  /// <summary>
  /// テスト用のクラス。
  /// </summary>
  /// <typeparam name="S">型</typeparam>
  /// <typeparam name="T">型</typeparam>
  internal class Test<S, T>
  ```

- SA1626: Single line comments must not use documentation style slashes.  
  XMLコメントで無い1行コメントには、スラッシュ3本のコメントを用いてはいけない。

- SA1627: Documentation text must not be empty.  
  XMLコメントのコメント文を空にしてはいけない。

- SA1628: (利用しない)

- SA1629: (利用しない)

- SA1630: (利用しない)

- SA1631: (利用しない)

- SA1632: (利用しない)

- SA1642: (利用しない)

- SA1643: (利用しない)

- SA1644: (利用しない)

- SA1646: Included documentation XPath does not exist.  
  XMLコメントに外部ドキュメントをインクルードするような指定において、存在しない、またはアクセスできないパスを指定してはいけない。

- SA1647: Include node does not contain valid file and path.
  XMLコメントに外部オブジェクトへのリンクをインクルードするような指定において、存在しない、またはアクセスできないパスを指定してはいけない。

- SA1648: Inherit doc must be used with inheriting class.  
  XMLコメントにおいて、継承されていないクラスの説明にInheritDocタグを用いてはいけない。

- SA1650: (利用しない)


### File Headers（ファイルヘッダー）

- SA1633: (利用しない)

- SA1634: (利用しない)

- SA1635: (利用しない)

- SA1637: (利用しない)

- SA1638: (利用しない)

- SA1639: (利用しない)

- SA1640: (利用しない)

- SA1649: (利用しない)


---
## Layout Rules（レイアウト関連ルール）

### Curly Brackets（カーリーブラケット: 中括弧）

- SA1500: Curly brackets for multiline statements must not share line.  
  複数行にわたる処理で、左中括弧や右中括弧は他のコードと同じ行に記載してはいけない。

  ```cs
  // 規約違反の例1（左中括弧が前の命令と同じ行にある）
  public object Method()
  {
      lock (this) {
          return this.value;
      }
  }

  // 規約違反の例2（右中括弧が前の命令と同じ行にある）
  public object Method()
  {
      lock (this) 
      {
          return this.value;}
  }

  // 正しい例
  public object Method()
  {
      lock (this) 
      {
          return this.value;
      }
  }
  ```


- SA1501: (利用しない)

- SA1502: (利用しない)

- SA1503: Curly brackets must not be omitted.  
  コードブロックを囲む中括弧を省略してはいけない。if文などで処理内容が1行ですむ場合であっても括弧で囲むこと。

  ```cs
  // 規約違反の例（ifの後の中括弧が省略されている）
  if (true) 
      return this.value;
  
  // 正しい例
  if (true) 
        {
            return this.value;
        }
  ```

- SA1504: All accessors must be multiline or singleline.  
  プロパティやイクデクサー、イベントの定義において、複数の子要素がある場合に片方を1行に書いたならば、もう片方も1行にしないといけない。また、片方を複数行に分けたならば他方も同様に複数に分けないといけない。

  ```cs
  // 規約違反の例（プロパティのgetだけを1行に、setは複数行にまたがっている）。
  public bool Enabled
  {
      get { return this.enabled; }
  
      set
      {
          this.enabled = value;
      }
  }

  // 正しい例1（どちらも1行にまとめた例）
  public bool Enabled
  {
      get { return this.enabled; }
      set { this.enabled = value; }
  }
  
  // 正しい例1（どちらも複数行に記述した例）
  public bool Enabled
  {
      get 
      { 
          return this.enabled; 
      }
      set 
      { 
          this.enabled = value; 
      }
  }
  ```


### Line Spacing（空行）

- SA1505: Opening curly brackets must not be followed by blank line.  
  左中括弧の次の行を開けてはいけない。

  ```cs
  // 規約違反の例（getの前とreturnの前に空行がある）
  public bool Enabled
  {

      get
      {

          return this.enabled;
      }
  }
  
  // 正しい例
  public bool Enabled
  {
      get
      {
          return this.enabled;
      }
  }
  ```

- SA1506: Element documentation headers must not be followed by blank line.  
  XMLコメントとメソッドやクラス宣言部との間に空行を入れてはいけない。

  ```cs
  // 規約違反の例

  /// <summary>
  /// Gets a value indicating whether the control is enabled.
  /// </summary>
  
  public bool Enabled
  {
      get { return this.enabled; }
  }
  ```

- SA1507: Code must not contain multiple blank line in a row.  
  2行以上の連続した空行を入れてはいけない。

  ```cs
  // 規約違反の例（returnの前に2行の空行）
  public bool Enabled
  {
      get 
      { 
          Console.WriteLine("Getting the enabled flag.");
  
  
          return this.enabled; 
      }
  }
  ```


- SA1508: Closing curly brackets must not be preceded by blank line.  
  右中括弧の前に空行を入れてはいけない。

  ```cs
  // 規約違反の例（returnのあとに空行）
  public bool Enabled
  {
      get 
      { 
          Console.WriteLine("Getting the enabled flag.");
          return this.enabled; 

      }
  }
  ```

- SA1509: Opening curly brackets must not be preceded by blank line.  
  左中括弧の前に空行を入れてはいけない。
  
  ```cs
  // 規約違反の例（getのあとに空行）
  public bool Enabled
  {
      get 

      { 
          Console.WriteLine("Getting the enabled flag.");
          return this.enabled; 
      }
  }
  ```

- SA1510: Chained statement blocks must not be preceded by blank line.  
  try～catchやif～elseのように中括弧のブロックが複数連なる場合に、前のブロックとの間に空行を入れてはいけない。
  
  ```cs
  // 規約違反の例（catchの前に空行）
  try
  {
      this.MethodCall();
  }

  catch (Exception ex)
  {
      Console.WriteLine(ex.ToString());
  }
  ```

- SA1511: While do footer must not be preceded by blank line.  
  do～while文においてwhileの前に空行を入れてはいけない。
  
  ```cs
  // 規約違反の例（whileの前に空行）
  do
  {
      Console.WriteLine("ループ中");
  }

  while (this.processing)
  ```

- SA1512: (利用しない)

- SA1513: (利用しない)

- SA1514: Element documentation header must be preceded by blank line.  
  XMLコメントの前には空行を入れないといけない。XMLコメントが前の宣言やメソッド等の末尾と隣接していてはいけない。

  ```cs
  // 規約違反の例（フィールド宣言と2つめのフィールド用XMLコメントの間に空行が無い）

  /// <summary>
  /// 活性化状態かどうかの判断用変数。
  /// </summary>
  private bool enabled = false;
  /// <summary>
  /// データへのアクセスキー情報。
  /// </summary>
  private string connectKeys = string.Empty;
  ```
  
- SA1515: (利用しない)

- SA1516: Elements must be separated by blank line.  
  複数行にまたがるタイプのコードブロック同士の間には空行を入れないといけない。ただし、1行に納める記法の場合はこの規約は適用されない。

  ```cs
  // 規約違反の例（複数行にまたがる記法のgetとsetが隣接している）
  public bool Enabled
  {
      get
      {
          return status;
      }
      private set
      {
          status = value;
      }
  }

  // 例外の例（1行に納めるスタイルの場合は隣接していてもよい）
  public string UserId
  {
      get { return userid; }
      set { userid = value; }
  }
  ```

- SA1517: Code must not contain blank lines at start of file.  
  ソースファイルの冒頭に空行を入れてはいけない。

- SA1518: Code must not contain blank lines at end of file.  
  ソースファイルの末尾に空行を入れてはいけない。


---
## Maintainability Rules（メンテナンス性関連ルール）

### Access Modifiers（アクセス修飾子）

- SA1400: Access modifier must be declared.  
  アクセス修飾子を省略してはいけない。クラス宣言などで省略されることが多いが、かならずpublicやprivateといったアクセス修飾子を明示的に記載すること。

- SA1401: Fields must be private.  
  フィールド（field、クラス内全般で利用できる変数）をprivate以外で宣言してはいけない。publicで宣言することでプロパティと同等の扱いにすることができるが、プロパティとの位置づけを明確化し、保守性を確保するためにpublicフィールドは利用禁止とする。


### Debug Text（デバッグテキスト）

- SA1404: Code analysis suppression must have justification.  
  コード分析機能でメッセージ表示を止める属性「SuppressMessage」を利用した場合には、必ずその理由を記述するjustification情報を記載しないといけない。

  ```cs
  // 規約違反の例
  [SuppressMessage("Microsoft.Performance", "CA1804:RemoveUnusedLocals")]
  public bool Enable()
  {
  }

  // 正しい例
  [SuppressMessage("Microsoft.Performance", 
    "CA1804:RemoveUnusedLocals", 
    Justification = "Used during unit testing")]
  public bool Enable()
  {
  } 
  ```

- SA1405: Debug assert must provide message text.  
  自動テストコードのアサート（assert）には、かならず説明文を記述しないといけない。

  ```cs
  // 正しい例
  Debug.Assert(value != true, "値は常にTrueであること。");
  ```

- SA1406: Debug fail must provide message text.  
  自動テストコードのフェイル（fail）には、かならず説明文を記述しないといけない。

  ```cs
  // 正しい例
  Debug.Fail("処理がこの箇所に到達してはいけない。");
  ```


### File Contents（ファイルコンテンツ）

- SA1402: File may only contain a single class.  
  1ファイルに複数のクラスを記述してはいけない。ただし、内部クラス（inner class、クラス宣言内部にクラスを宣言したもの）や内部インターフェース（inner interface）は対象外。

- SA1403: File may only contain a single namespace.  
  1ファイルに複数の名前空間（namespace）を記述してはいけない。


### Parenthesis（括弧）

- SA1119: Statement must not use unnecessary parenthesis.  
  式の記述において不要な括弧が含まれてはいけない。

  ```cs
  // 規約違反の例（xの右辺、加算を囲む括弧不要。yの右辺、Methodを囲む括弧は不要）
  int x = (5 + b);
  string y = (this.Method()).ToString();
  
  // 正しい例
  int x = 5 + b;
  string y = this.Method().ToString();
  ```

- SA1407: Arithmetic expressions must declare precedence.  
  演算順位を考慮する必要がある数式には、優先すべき演算を明示するための括弧を記載しないといけない。数式としてC#のコンパイラーは順位を自動的に認識して式を解釈するが、読み手の人間は必ずしも正しく読み解けるとは限らないことから、たとえば乗算や除算と加減算が混在するような数式では、乗算、除算の周囲を括弧で囲むなどの可読性対策を行う必要がある。

  ```cs
  // 規約違反の例
  int x = 5 + y * b / 6 % z - 2;

  // 正しい例
  int x = 5 + (y * ((b / 6) % z)) - 2;
  ```  

- SA1408: Conditional expressions must declare precedence.  
  条件文（if文などの条件式）において、比較演算順序が重要な場合の括弧は記載しないといけない。
  
  ```cs
  // 規約違反の例
  if (x || y && z && a || b)
  
  // 正しい例1
  if ((x || y) && z && (a || b))
  
  // 正しい例2
  if (x || (y && z && a) || b)
  ```  

- SA1410:Remove delegate parenthesis when possible.  
  デリゲート（delegate）の匿名メソッド（anonymous method）が引数を持たない場合に、引数の括弧を記載してはいけない。

  ```cs
  // 規約違反の例
  this.Method(delegate() { return 2; });

  // 正しい例
  this.Method(delegate { return 2; });
  ```

- SA1411: Attribute constructor must not use unnecessary parenthesis.  
  クラス等の属性（attribute）定義において、中身が空の括弧を記載してはいけない。
  
  ```cs
  // 規約違反の例（属性 Serializableの後ろに空の括弧がある）
  [Serializable()]
  public class ExportData
  {
  }

  // 正しい例
  [Serializable]
  public class ExportData
  {
  }
  ```


### Removable Code（不要コード）

- SA1409: Remove unnecessary code.  
  無駄なコードは削除しないといけない。ここでいう無駄なコードとは下記のように中身が空のコードブロックを指す。
   - 中身が空のtry/catch。
   - finally句が空のtry/finally。
   - 中身が空のusingやunsafe。


---
## Naming Rules（命名規則）

- SA1300: Element must begin with upper case letter.  
  メソッド名の先頭は大文字でないといけない。下記のようなイベントプロシージャの場合でも同様。コントロール名の冒頭を小文字にしていたとしても、イベントプロシージャとするメソッド名の冒頭は大文字に書き換えること。

  ```cs
  // 規約違反の例
  private void buttonLogin_Click(object sender, EventArgs e)
  ```

- SA1301: （利用しない。現時点で適用できる状況は発生しない。）

- SA1302: Interface names must begin with I.  
  インターフェース定義の名称は必ず大文字の「I（アイ）」で始めないといけない。

- SA1303: Const field names must begin with upper case letter.  
  定数宣言の定数名は必ず大文字で始めないといけない。

- SA1304: Non private readonly fields must begin with upper case letter.  
  プライベート（private）でない読み取り専用フィールド（readonly fields）の名称は必ず大文字で始めないといけない。

- SA1305: Field names must not use Hungarian notation.  
  フィールド名にハンガリアン記法を用いてはいけない（訳注: ハンガリアン記法と見なす文字列は別途設定されている）。下記の例の場合、変数名の「db」が規約違反。

  ```cs
  // 規約違反の例
  string dbConnectString = this.GetSettings(ConnectString);
  ```

- SA1306: Field names must begin with lower case letter.  
  フィールドの名称（訳注: private宣言されたフィールドの名称）は必ず小文字で始めないといけない。

- SA1307: Accessible field must begin with upper case letter.
  外部に公開されているフィールド名は必ず大文字で始めないといけない（本件はpublicフィールドを禁じている時点で該当するケースは発生しない)。

- SA1308: Variable names must not be prefixed.  
  変数名にプリフィックス（接頭語）をつけてはいけない。旧来のプログラミングで見られた、「m_conncectString」や「szFileName」といった接頭語をつけることは禁止する。

- SA1309: (利用しない)

- SA1310: (利用しない)

- SA1311: Static readonly fields must begin with upper case letter.  
  静的読み取り専用フィールド（static readonly fields）の名称は大文字で始めないといけない。


---
## Ordering Rules（記述順序関連ルール）

### Element Order（記述要素の順序）

- SA1200: (利用しない)

- SA1201: Elements must appear in the correct order.  
  ソースファイルを構成する各要素の記述順序が下記の通りでないといけない。  
  [ファイル全体の記述順序]
   ・ 外部エイリアスディレクティブ（Extern alias directives）。
   ・ using句（Using directives）。
   ・ 名前空間宣言（Namespaces）。 
   ・ 列挙型宣言（Enums）。
   ・ インターフェース宣言（Interfaces）。
   ・ 構造体宣言（Structs）。
   ・ クラス宣言（Classes）。
  [クラス/インターフェース/構造体内部の記述順序]
   ・ フィールド（Fields）。
   ・ コンストラクター（Constructors）。
   ・ ファイナライザーとデストラクタｰ（Finalizers and Destructors）。
   ・ デリゲート（Delegates）。
   ・ イベント（Events）。
   ・ 列挙型（Enums）。
   ・ インターフェース（Interfaces）。
   ・ プロパティ（Properties）。
   ・ インデクサｰ（Indexers）。
   ・ メソッド（Methods）。
   ・ 構造体（Structs）。
   ・ クラス（Classes）。

- SA1202: Elements must be ordered by access.  
  ソース上の各要素は下記のアクセス修飾子順に記述されていないといけない。  
   1. public
   1. internal
   1. protected internal
   1. protected
   1. private

- SA1203: Constants must appear before fields.  
  定数宣言（constant field）はフィールド変数の前に記述されないといけない。大まかな挙動においてフィールドと定数は似た扱いになるが、コンパイラーおよび処理内部におけるデータ保持方法は根本的に異なることから、これらは異なるものとしてまとめることが望ましいためである（デリケートな処理やプログラム構造においてはこれらを厳密に区別する必要性がある）。

- SA1204: Static elements must appear before instance elements.  
  static宣言された要素は、staticでない通常の要素よりも前に記述されていないといけない。

- SA1206: Declaration keywords must follow order.  
  メソッド等様々な要素の宣言部分は、必ず「アクセス修飾子（access modifiers）」→「static宣言（必要な場合のみ）」→「その他、名称や引数」の順序で記述されないといけない。

- SA1207: Protected must come before internal.  
  「protected internal」宣言を「internal protected」と記述してはいけない。

- SA1212: Property accessors must follow order.  
  プロパティ宣言において、getはsetの前に記述しないといけない。

  ```cs
  // 規約違反の例
  public string Name
  { 
      set { this.name = value; }
      get { return this.name; }
  }
  
  // 正しい例
  public string Name
  { 
      get { return this.name; }
      set { this.name = value; }
  }
  ```

- SA1213: Event accessors must follow order.  
  イベントの宣言において、addはremoveの前に記述しないといけない。

  ```cs
  // 規約違反の例
  public event EventHandler NameChanged
  { 
      remove { this.nameChanged -= value; }
      add { this.nameChanged += value; }
  }
  
  // 正しい例
  public event EventHandler NameChanged
  { 
      add { this.nameChanged += value; }
      remove { this.nameChanged -= value; }
  }
  ```

- SA1214: Static readonly elements must appear before static non readonly elements.  
  static readonly宣言されたフィールドは、readonlyでないフィールドよりも前に記述しないといけない。

- SA1215: Instance readonly elements must appear before instance non readonly elements. 
  readonly宣言されたフィールドは、readonlyでないフィールドよりも前に記述しないといけない。


### Using Directives（Using句）

using句の並び順に関するルールは可読性や誤読回避のために設けられている。順序が動作に影響することはないが規則に従うこと。なお、順序についてはVisual Studioの設定で[テキストエディター]-[C#]-[詳細設定]にある「using句を並べ替える際に、'System'ディレクティブを先頭に配置する」をONにした上で、メニューバーの[編集]-[IntelliSense]-[usingの削除と並べ替え]を実施することで自動的に最適化される。

- SA1208: System using directives must be placed before other using directives.  
  using句において「System.」で始まる名前空間（namespace）宣言は宣言の先頭に記述されないといけない。

- SA1209: Using alias directives must be placed after other using directives.  
  usingのエイリアス設定（using-alias directives）は、エイリアス設定されていない普通のusing句の前に記述されないといけない。

- SA1210: (利用しない)

- SA1211: Using alias directives must be ordered alphabetically be alias namespace.  
  using句のエイリアス設定（using-alias directives）は、アルファベット順に並んでいないといけない。

- SA1216: Using static directives must be placed at the correct location.  
  using static宣言（using static directives）は通常のusing句とusing句のエイリアス設定の間に記述されていないといけない。


---
## Readability Rules（可読性関連ルール）

###  Comments（コメント）

- SA1120: Comments must contain text.  
  空のコメントを記述してはいけない。コメントを表す「//」の後には必ずコメント文を記述する。コメント文が無いならば「//」自体も消去しないといけない。


### Member Access（クラスメンバーのアクセス修飾子）

- SA1100: Do not prefix calls with base unless local implementation exists.  
  継承されたクラスにおいて、継承先でオーバーライド（override）や抽象メンバーの実装（implementation）が無い場合に、メンバーに「base.」をつけてはいけない。ベースクラスにしか無いメソッドを呼ぶ場合は「base.」でなく「this.」を使う。逆に「base.」を使うのは、継承先と継承元の両方に同名のメンバーがあり、どちらを呼ぶか明示的に指定する必要がある場合のみとする。

- SA1101: (利用しない)

- SA1126: Prefix calls correctly.  
  メンバーのプリフィックスを適切に（必ず）記述する。クラスのメンバーであるメソッドやプロパティ、フィールドの場合は「this」、継承元クラスの場合は「base」などを必ず記述する。

### Method Parameter Placement（メソッド引数の記述）

- SA1110: Opening parenthesis must be on declaration line.  
  メソッド宣言部を複数行に分割する場合、引数冒頭の左括弧はメソッドの宣言と同じ行に記述しないといけない。この規約は宣言時だけではなくメソッド呼び出し時にも適用される。

  ```cs
  // 規約違反の例
  private int GetProcessedValue
      (
          int value1, 
          int value2, 
          int value3)
  
  // 正しい例（ただし、宣言を複数行に分割する場合）
  private int GetProcessedValue(
          int value1, 
          int value2, 
          int value3)
  ```

- SA1111: Closing parenthesis must be on line of last parameter.  
  メソッド宣言部を複数行に分割する場合、引数最後の右括弧は引数と同じ行に記述しないといけない。この規約は宣言時だけではなくメソッド呼び出し時にも適用される。

  ```cs
  // 規約違反の例
  private int GetProcessedValue(
          int value1, 
          int value2, 
          int value3
        )
  {
  }
  
  // 正しい例（ただし、宣言を複数行に分割する場合）
  private int GetProcessedValue(
          int value1, 
          int value2, 
          int value3)
  {
  }
  ```

- SA1112: Closing parenthesis must be on line of opening parenthesis.  
  引数が無いメソッド宣言や呼び出しの際に、メソッド末尾の括弧は同じ行に記載されないといけない。

  ```cs
  // 規約違反の例
  private int GetValue(
      )
  {
      int result = 0;
  }
  
  // 正しい例
  private int GetValue()
  {
      int result = 0;
  }
  ```
- SA1113: Comma must be on same line as previous parameter.  
  メソッド宣言や呼び出し等で引数を複数行に記載する場合、各引数の区切りカンマは引数の後ろに記載しないといけない。

  ```cs
  // 規約違反の例
  int[] valueList = GetValues(
                          sample1
                          , sample2);
  
  // 正しい例
  int[] valueList = GetValues(
                          sample1, 
                          sample2);
  ```

- SA1114: Parameter list must follow declaration.  
  メソッド宣言や呼び出しにおいて、メソッド名部分と引数記述部の間に空行をいれてはいけない。

  ```cs
  // 規約違反の例
  private void Form1_Load(
  
      object sender, EventArgs e)
  {
  
  // 正しい例（メソッド名と同じ行の例）
  private void Form1_Load(object sender, EventArgs e)
  {
  
  // 正しい例（メソッド名と引数部を分けた例）
  private void Form1_Load(
      object sender, EventArgs e)
  {
  ```

- SA1115: parameter must follow comma.  
  メソッド宣言や呼び出しにおいて、各パラメーターの間に空行を入れてはいけない。

  ```cs
  // 規約違反の例
  private void Form1_Load(
      object sender, 
      
      EventArgs e)
  {
  
  // 正しい例（メソッド名と同じ行の例）
  private void Form1_Load(object sender, EventArgs e)
  {
  
  // 正しい例（メソッド名と引数部を分けた例）
  private void Form1_Load(
      object sender, EventArgs e)
  {
  ```  

- SA1116: Split parameters must start on line after declaration.  
  メソッド宣言や呼び出しにおいて、引数を複数行に分割して記述する場合、先頭引数はメソッド名の次の行から書き始めないといけない。

  ```cs
  // 規約違反の例
  private void Form1_Load(object sender, 
      EventArgs e)
  {
  
  // 正しい例(2行目にすべてまとめた場合)
  private void Form1_Load(
      object sender, EventArgs e)
  {
  
  // 正しい例(複数行二分割した場合)
  private void Form1_Load(
      object sender, 
      EventArgs e)
  {
  ```   

- SA1117: Parameter must be on same line or separate lines.  
  メソッド宣言や呼び出しにおいて、引数はすべて1行に記述するか、すべてを別の行に分割するかのどちらかで記述しないといけない。

  ```cs
  // 規約違反の例（分割したりしなかったりが混在している）
  this.UpdateProductTable(
      keyValue, 
      prodName, string.Empty, 
      12, DateTime.Now);
  
  // 正しい例（すべての引数を1行にまとめた場合）
  this.UpdateProductTable(keyValue, prodName, string.Empty, 12, DateTime.Now);
  
  // 正しい例（すべてを分割した場合）
  this.UpdateProductTable(
      keyValue, 
      prodName, 
      string.Empty, 
      12, 
      DateTime.Now);
  ```

- SA1118: Parameter must not span multiple lines.  
  1つの引数を複数行にまたいで記述してはいけない。

  ```cs
  // 規約違反の例（文字列結合している引数を2行に分割している）
  this.UpdateProductTable(
      keyValue, 
      "[完成品]" +
      prodName, 
      string.Empty, 
      12, 
      DateTime.Now);
  
  // 正しい例（すべてを1行にまとめた場合）
  this.UpdateProductTable(keyValue, "[完成品]" + prodName, string.Empty, 12, DateTime.Now);
  
  // 正しい例（すべての引数を分割した場合）
  this.UpdateProductTable(
      keyValue, 
      "[完成品]" + prodName, 
      string.Empty, 
      12, 
      DateTime.Now);
  ```

### Query Expressions（LINQクエリー記述）

- SA1102: Query clause must follow previous clause.  
  クエリー文を記述する際には各命令は必ず前の命令語や行に連ね、空行を入れてはいけない。下記の例ではwhere句の前に空行が入っているため規約違反となる。

  ```cs
  // 規約違反の例
  List<int> valueList = new List<int>() { 1, 2, 3, 51, 100 };
  var result = from item in valueList

      where item > 50 select item;

  // 正しい例（1行にまとめた場合）
  List<int> valueList = new List<int>() { 1, 2, 3, 51, 100 };
  var result = from item in valueList where item > 50 select item;

  // 正しい例（複数行に分割した場合）
  var result = 
      from item in valueList 
      where item > 50 
      select item;
  ```

- SA1103: Query clauses must be on separate lines or all on one line.  
  クエリー文は1行に集約するか、文節ごとに行を分けるかのいずれかで記述しないといけない。以下のような記述は、fromとwhereが分かれているのにwhereとselectが1行にまとまっていることから一貫性が無く、規約違反となる。

  ```cs
  // 規約違反の例（whereの前で改行している）
  List<int> valueList = new List<int>() { 1, 2, 3, 51, 100 };
  var result = from item in valueList
      where item > 50 select item;
  ```

- SA1104: Query clauses must begin on new line when previous clause spans multiple lines.  
  クエリー句の前の行が複数に分割されている場合、続くクエリー句は前の行から改行して新しい行で開始されないといけない。クエリー中にメソッドなどが含まれる場合下記のGetValuesのように行分割できるが、続くクエリー句のwhereは前の括弧から改行しなければ規約違反となる。

  ```cs
  // 規約違反の例
  var result = from item in this.GetValues(
    2, 
    5)  where item > 50
        select item;

  // 正しい例
  var result = from item in this.GetValues(
    2, 
    5)
        where item > 50
        select item;
  ```

- SA1105: Query clauses spanning multiple lines must begin on own line.  
  クエリーの一部、特にクエリー中で呼ばれるメソッドなどの引数が複数行に分割される場合、そのメソッドを呼んでいるクエリー句は前の句と行を分けないといけない（訳注: この規約はSA1103を守っていれば大抵カバーされるのであまり気にする必要は無い）。参考までに以下に例を記載する。

  ```cs
  // 規約違反の例
  var result = from item in values where item > 50 select this.GetValues
             (
                item, 25);

  // 正しい例（といってもSA1103には違反してしまうため、さらに行分割が必要）
  var result = from item in values where item > 50 
        select this.GetValues
             (
                item, 25);
  ```


### Regions（Region記述）

- SA1123: (利用しない)

- SA1124: (利用しない)


### Statements（命令語）

- SA1106: Code must not contain empty statements.  
  コードには空のステートメントを含めてはいけない。例えばセミコロンのみの行などがこれに相当する。誤記でセミコロンが2つ連なったような場合も該当する（コンパイルエラーにはならない）。

- SA1107: Code must not contain multiple statements on one line.  
  1行に2つ以上の命令をセミコロンでつないで記述してはいけない。

- SA1108: Block statements must not contain embedded comments.  
  ブロックを記述する文法中にコメントを埋め込んではいけない。コメントを書くならばブロックを表す中括弧「{}」の中にする。下記のelse句にある「その他の場合」のようなコメントはNG。elseの括弧内部に移すこと。

  ```cs
  // 規約違反の例
  if (targetValue >= 0)
  {
      // プラスの場合
      positive++;
  }
  // マイナスの場合 ← この場所にコメントを入れてはいけない
  else
  {
      negative++;
  }

  // 正しい例
  if (targetValue >= 0)
  {
      // プラスの場合
      positive++;
  }
  else
  {
      // マイナスの場合
      negative++;
  }
  ```

- SA1109: Block statements must not contain embedded regions.  
  ブロックを記述する文法中にregionを埋め込んではいけない。regionを書くならばブロックを表す中括弧「{}」の中にする。下記のelse句にある「異常ケース」のようなregionはNG。elseの括弧内部に移すこと。

  ```cs
  // 規約違反の例
  if (targetValue >= 0)
  {
      // プラスの場合
      positive++;
  }
  #region 異常ケース
  else
  {
      negative++;
  }
  #endregion
  ```

- SA1131: Use readable conditions.  
  条件式は読みやすく記述する。たとえば変数とリテラルの比較において「 1 > sampleValue 」のように右辺に変数が来るような記述はこの規約に違反する。このようなケースでは変数は左辺に記述する。


### Strings（文字列）

- SA1122: Use string.Empty for empty strings.  
  空文字にはstring.Emptyを利用する。“”を使わない。ただし、定数宣言で空文字を定義する場合はstring.Emptyが使えないので、定数の宣言は例外とする。


### Types（型）

- SA1121: Use built in type alias.  
  組み込みのエイリアス名がある型は完全修飾名を使ってはいけない。C#の組み込みデータ型には、.NET Frameworkの定めるフルネーム（fully qualified name: 完全修飾名）や名前空間を略した型名と、C言語などとの互換性を意識したエイリアス（type alias）がある。StyleCopでは全体を統一するためにエイリアス名がある型についてはそちらを利用するよう規定している。エイリアスと型名、完全修飾名の対応は下記の通り。  
  
|エイリアス名|型名|完全修飾名|
|---|---|---|
|bool|Boolean|System.Boolean|
|byte|Byte|System.Byte|
|char|Char|System.Char|
|decimal|Decimal|System.Decimal|
|double|Double|System.Double|
|short|Int16|System.Int16|
|int|Int32|System.Int32|
|long|Int64|System.Int64|
|object|Object|System.Object|
|syte|SByte|System.SByte|
|float|Single|System.Single|
|string|String|System.String|
|ushort|UInt16|System.UInt16|
|uint|UInt32|System.UInt32|
|ulong|UInt64|System.UInt64|

- SA1125: Use shorthand for nullable types.  
  null許容型を用いる場合は短縮形表記でないといけない。値型の変数を引数に持つメソッドなどで、データベース処理結果などの都合でnull型が引き渡される可能性がある場合など、本来はnullが入らない値型にnullを渡すために「Nullable<T>」という特殊な型を用いることができる。しかし、この型には型名に「?」をつけた短縮形が定義されているのでそちらを利用すること。 

  ```cs
  // 規約違反の例
  private string ConvertDate2String(Nullable<DateTime> targetDate)
  {
      string result = string.Empty;
  
  // 正しい例
  private string ConvertDate2String(DateTime? targetDate)
  {
      string result = string.Empty;
  ```

---
## Spacing Rules（空白に関するルール）

- SA1000: Keywords must be spaced correctly.  
  命令語は適切に空白で区切られていないといけない。詳細は下記の通り。
   
   - 下記のC#命令の後ろには空白が必要。  
     await、case、catch、fixed、for、foreach、from、group、if、in、into、join、let、lock、orderby、out、ref、return、select、stackalloc、switch、using、var、where、while、yield
   - 下記のC#命令の前には空白が必要。  
     checked、default、sizeof、typeof、unchecked
   - newの前には空白が必要だが、下記の場合は例外として除く。
     
     - 配列を初期設定する場合の中括弧の中のnew。
     - ジェネリック型宣言の中のnew。

- SA1001: Commas must be spaced correctly.  
  カンマ周辺の空白が適切に記述されていないといけない。下記の場合を除き、カンマの後ろには必ず空白を配置しないといけない。

   - カンマが行末にある場合。
   - ジェネリック型指定（open generic typeという）の型宣言内（Dictionary<string,int>のようなケース）。
   - 文字列補完（string interpolation）や書式設定の設定中にあるカンマ（$"{x,3}"のようなケース）。

- SA1002: Semicolons must be spaced correctly.  
  セミコロン周辺の空白が適切に記述されていないといけない。下記の場合を除き、セミコロンの後ろには空白を配置しないといけない。

   - セミコロンが行末にある場合。
   - セミコロンの次が閉じ括弧の場合。

- SA1003: Symbols must be spaced correctly.  
  演算子の前後は空白でないといけない。ここでいう演算子とは、コロン、算術演算子、代入演算子（イコール等）、条件演算子（不等号等）、論理演算子、シフト演算子およびラムダ演算子を指す。ただし、以下のものやケースについては例外とする。

   - 単項演算子の場合（Notを表す!など）の演算子の後ろ。
   - 変数や演算子が括弧で囲われている場合の括弧との間。

- SA1004: Documentation lines must begin with single space.  
  XMLコメントの冒頭には空白を入れないといけない。スラッシュ3つのコメント記号のあと、空白なしでコメント文を書き始めてはいけない。

- SA1005: (利用しない)

- SA1006: Preprocessor keywords must not be preceded by space.  
  プリプロセッサー記号（#regionなどの#）のあとに空白をいれてはいけない。

- SA1007: Operator keyword must not be preceded by space.  
  演算子のオーバーロード定義において、宣言部分の演算子の前に空白をいれてはいけない。たとえば加算演算子「+」をオーバーロードで追加定義する場合、「operator」句と「+」は続けて記述しなければならない（詳細は演算子のオーバーロードに関する説明等を参考にすること）。

- SA1008: Opening parenthesis must be spaced correctly.  
  左括弧の前に空白をいれてはいけない。ただし、if、while、forといった命令の後や、数式中の演算子に続く場合、また、行のはじめに括弧がある場合は例外とする。

- SA1009: Closing parenthesis must be spaced correctly.  
  右括弧（閉じ括弧）の前に空白を入れてはいけない。また、一部の例外を除き、閉じ括弧の右側には空白を入れないといけない（キャストの型指定部分などは除く）。

- SA1010: Opening square brackets must be spaced correctly.  
  左大括弧（opening square bracket）の前後に空白を入れてはいけない。ただし行頭や行末に記述される場合は例外とする。

- SA1011: Closing square brackets must be spaced correctly.  
  右大括弧（closing square bracket）の後ろには必ず空白を入れないといけない。ただし下記の場合は例外とする。

   - 行末にある場合。
   - 別の右括弧や、別の左括弧が続く場合。
   - カンマやセミコロンが続く場合。
   - 文字列補完（string interpolation）の書式文字列中にある場合（例: $"{x[i]:C} のような場合）。
   - ある種の演算子などが続く場合（インクリメントなど）。

- SA1012: (利用しない)

- SA1013: (利用しない)

- SA1014: Opening generic brackets must be spaced correctly.  
  ジェネリック型の宣言で用いる左括弧の前後に空白を入れてはいけない。

  ```cs
  // 規約違反の例（Listと<int>の間に空白がある）
  private List <int> GetValues(string targetCode)
  {

  // 正しい例（Listと<int>の間に空白がない）
  private List<int> GetValues(string targetCode)
  {      
  ```

- SA1015: Closing generic brackets must be spaced correctly.  
  ジェネリック型の宣言で用いる右括弧の前に空白を入れてはいけない。

  ```cs
  // 規約違反の例（<int>の右括弧前に空白がある）
  private List<int > GetValues(string targetCode)
  {

  // 正しい例
  private List<int> GetValues(string targetCode)
  {      
  ```

- SA1016: Opening attribute brackets must be spaced correctly.  
  属性記述の左括弧の後ろに空白を入れてはいけない。
  
  ```cs
  // 規約違反の例（Description属性左端の大括弧後に空白が入っている）
  [Category(CATEGORY_DISPLAY)]
  [ Description("線のサイズを取得、または設定します。")　]
  [DefaultValue(1)]
  public int BorderSize
  {
      get { return _borderSize; }
  
  // 正しい例
  [Category(CATEGORY_DISPLAY)]
  [Description("線のサイズを取得、または設定します。")]
  [DefaultValue(1)]
  public int BorderSize
  {
      get { return _borderSize; }
  ```

- SA1017: Closing attribute brackets must be spaced correctly.
  属性記述の右括弧の前に空白を入れてはいけない。
  
  ```cs
  // 規約違反の例（Description属性右端の大括弧前に空白が入っている）
  [Category(CATEGORY_DISPLAY)]
  [Description("線のサイズを取得、または設定します。")　]
  [DefaultValue(1)]
  public int BorderSize
  {
      get { return _borderSize; }
  
  // 正しい例
  [Category(CATEGORY_DISPLAY)]
  [Description("線のサイズを取得、または設定します。")]
  [DefaultValue(1)]
  public int BorderSize
  {
      get { return _borderSize; }
  ```

- SA1018: Nullable type symbols must not be preceded by space.  
  null許容型を表す「?」の前に空白を入れてはいけない。

  ```cs
  // 規約違反の例（intの後ろに空白が入っている）
  private string GetProductName(int ? prodCode)
  {

  // 正しい例
  private string GetProductName(int? prodCode)
  {
  ```

- SA1019: Member access symbols must be spaced correctly.  
  クラス等のメンバー（メソッドやプロパティなど）を呼び出す際のピリオド記号（member access symbol）の前後に空白を入れてはいけない。

  ```cs
  // 規約違反の例（stringとEmptyをつなぐピリオドの前後に空白がある）
  string resultName = string . Empty;

  // 正しい例
  string resultName = string.Empty;
  ```
- SA1020: Increment decrement symbols must be spaced correctly.  
  インクリメント/デクリメント演算子と変数の間に空白を入れてはいけない。

- SA1021: Negative signs must be spaced correctly.  
  行頭や左括弧の後で無ければ、マイナス符号の前は必ず空白をいれないといけない。また、マイナス符号の後ろに空白を入れてはいけない。

- SA1022: Positive signs must be spaced correctly.  
  行頭や左括弧の後で無ければ、プラス符号の前は必ず空白をいれないといけない。また、プラス符号の後ろに空白を入れてはいけない。

- SA1023: Dereference and access-of symbols must be spaced correctly.  
  ポインター型を表す「＊」（deference/access-of symbol）の前後の空白を適切に配意しないといけない。型宣言時の「＊」の前に空白を入れてはいけない。また、ポインター型変数を表す「＊」の後ろに空白を入れてはいけない。

  ```cs
  // 規約違反の例（int* に不正な空白。また、*pointerにも不正な空白）
  unsafe
  {
      int[] parameters = new int[] { 1, 10, 20, 30, 50 };
      fixed (int * pointer = &parameters[0])
      {
          * pointer += 1;
      }
  }
  
  // 正しい例
  unsafe
  {
      int[] parameters = new int[] { 1, 10, 20, 30, 50 };
      fixed (int* pointer = &parameters[0])
      {
          *pointer += 1;
      }
  }
  ```

- SA1024: Colons must be spaced correctly.  
  コード中のコロン前後の空白を適切に設置しないといけない。
   
   - コンストラクター初期化子や継承関係の宣言、インターフェース実装の宣言などにおけるコロンは、前後に必ず空白を配置しないといけない。
   - 文字列補完（string interpolation）の書式文字列中にある場合（例: $"{x[i]:C} のような場合）、コロン前後に空白を入れてはいけない。
   - ラベル定義やswitch文でのcase宣言の場合、コロンの前に空白をいれてはいけない。
   - 三項演算子におけるコロンでは前後に必ず空白を入れないといけない。

- SA1025: Code must not contain multiple whitespace in a row.  
  コード中に2行以上の連続した空行を入れてはいけない。

- SA1026: Code must not contain space after new keyword in implicitly typed array allocation.  
  配列宣言での初期値設定において、型指定なしの暗黙の配列宣言を行う場合のnewと大括弧の間には空白を入れてはいけない。

  ```cs
  // 規約違反の例（newと[]の間に空白がある）
  int[] parameters = new [] { 1, 10, 20, 30, 50 };
  
  // 正しい例
  int[] parameters = new[] { 1, 10, 20, 30, 50 };
  ```

- SA1027: (利用しない)

- SA1029: Do not split null conditional operators.  
  Null条件演算子（null-conditional operator）の前後に空白などを挿入してはいけない。
  
  ```cs
  // 規約違反の例（?とピリオドのあいだに空白がある）
  return prodCode? .ToString();
  
  // 正しい例
  return prodCode?.ToString();
  ```

---

# 参考資料

- [DotNetAnalyzers/StyleCopAnalyzers/documentation@GitHub](https://github.com/DotNetAnalyzers/StyleCopAnalyzers/tree/master/documentation)

---
